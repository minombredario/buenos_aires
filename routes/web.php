<?php

/*Route::get('/admin', function(){
      return view('home');
  });//->middleware('auth');
*/
//Route::get( '/admin/login', 'API\AuthController@getLogin' );
      //->middleware('auth');
/*
//Route::get('/logout', 'API\AuthController@getLogout');
*/


Route::group(['middleware' => 'web'], function () {
      //Route::get('/{any}', 'HomeController@index')->where('any', '.*');
      //Route::get('/admin', 'HomeController@index');
      Route::get('/', 'HomeController@getApp');
      //Route::get( '/admin', 'API\AuthController@getApp' )->name('home');
      Route::get('admin/{vue_capture?}', 'HomeController@index')->where('vue_capture','[\/\w\.-]*');
      Route::get('signup/activate/{token}', 'API\AuthController@signupActivate');
});

/*
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
*/