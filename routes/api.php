<?php

use Illuminate\Http\Request;

/*
  Public API Routes
*/
/*Route::post('/register', 'AuthController@register');
Route::post('/login', 'AuthController@login');

Route::middleware('auth:api')->group(function () {
    Route::post('/logout', 'AuthController@logout');
    Route::get('/user', 'AuthController@user');
});*/

/*Route::group(['prefix' => 'v1/auth'], function () {
  Route::post('login', 'AuthController@login');
  Route::post('signup', 'AuthController@signup');
  Route::group(['middleware' => 'auth:api'], function() {
      Route::get('logout', 'AuthController@logout');
      Route::get('user', 'AuthController@user');
  });
});*/


//Route::post('login', 'API\AuthController@login');

/*Route::group(['prefix' => 'v1'], function(){


  
});*/



Route::post('register', 'API\AuthController@register');

Route::group(['namespace' => 'Auth', 'middleware' => 'api', 'prefix' => 'password'], function () {    
  Route::post('create', 'API\PasswordResetController@create');
  Route::get('find/{token}', 'API\PasswordResetController@find');
  Route::post('reset', 'API\PasswordResetController@reset');
});

Route::group(['prefix' => 'v1'], function(){
  Route::patch('user/{user}', 'API\UsersController@updateUser')->middleware('can:update,user');
  Route::get('getDownload', 'API\DownloadController@getDownload');
});


Route::group(['prefix' => 'v1/admin'], function(){
  Route::post('login', 'API\AuthController@login'); 
  Route::post('signup', 'API\AuthController@signup');
  
  //Route::get('/', 'API\AuthController@getLogin');
  //Route::post('/login', 'API\AuthController@login');
});

/*
  Authenticated API Routes.
*/
Route::group(['prefix' => 'v1/admin', 'middleware' => ['auth:api', 'worker']], function(){
  Route::get('logout', 'API\AuthController@logout');

  Route::get('usersStats', 'API\Admin\StatsController@usersStats');
  Route::get('users', 'API\Admin\UsersController@getUsers');
  Route::get('getUser', 'API\Admin\UsersController@getUser');
  Route::get('user', 'API\AuthController@user'); 
  Route::post('addUser', 'API\AuthController@register');

  Route::patch('toggleActive/{id}', 'API\Admin\UsersController@toggleActive');

  
  
});

/*
  Worker Routes. Debe ser al menos trabajador para acceder a estas rutas.
*//*
Route::group(['prefix' => 'v1/admin', 'middleware' => ['auth:api', 'worker']], function(){
  
});*/

/*
  Admin Routes
*/
/*
Route::group(['prefix' => 'v1/admin', 'middleware' => ['auth:api', 'admin']], function(){
  Route::post('/login', 'API\AuthController@login');
  Route::post('/logout', 'API\AuthController@getLogout');
  Route::get('/user', 'API\Admin\UsersController@getUser');
});*/

/*
  Super-Admin Routes
*/
/*Route::group(['prefix' => 'v1/admin', 'middleware' => ['auth:api', 'super-admin']], function(){

  Route::get('/actions', 'API\Admin\ActionsController@getActions');
  
  Route::put('/actions/{action}/approve', 'API\Admin\ActionsController@putApproveAction')->middleware('can:approve,action');
  
  Route::put('/actions/{action}/deny', 'API\Admin\ActionsController@putDenyAction')->middleware('can:deny,action');

});*/