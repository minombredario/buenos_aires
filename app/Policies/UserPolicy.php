<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class UserPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Si el usuario es un admin o un superadmin, puede actualizar a los usuarios.
     */
     public function update( User $user ){
      
      if( $user->permission == 1 || $user->permission == 2 || $user->permission == 3 ){
        return true;
      }else{
        return false;
      }
     }


    /**
     * Si el usuario es un admin o un superadmin, puede agregar otros admins
     */
    public function addAdmins( User $user ){
      if( $user->permission == 2 || $user->permission == 3 ){
        return true;
      }else{
        return false;
      }
    }

    /**
     * Si el usuario es un superadmin, puede agregar otros superadmins.
     */
    public function addSuperAdmins( User $user ){
      if( $user->permission == 3 ){
        return true;
      }else{
        return false;
      }
    }
}
