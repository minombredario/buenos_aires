<?php

namespace App\Policies;

use App\Models\User;
use App\Models\Action;

use Illuminate\Auth\Access\HandlesAuthorization;

class ActionPolicy
{
    use HandlesAuthorization;

    /**
     * Create a new policy instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }
    public function approve( User $user, Action $action ){
        if( $user->permission == 2 || $user->permission == 3 ){
            return true;
        }else if( $user->clubsOwned->contains( $action->club_id ) ){
          return true;
        }else{
          return false;
        }
    }
      
    public function deny( User $user, Action $action ){
        if( $user->permission == 2 || $user->permission == 3 ){
          return true;
        }else if( $user->clubsOwned->contains( $action->club_id ) ){
          return true;
        }else{
          return false;
        }
      }
}
