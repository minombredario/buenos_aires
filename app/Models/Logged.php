<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Logged extends Model
{
    protected $table = 'loggeds';

    protected $fillable = [
        'user_id', 'online', 'current_login_time', 'last_logged_at'
    ];

    protected $hidden = [
        'updated_at', 'created_at', 'user_id', 'current_login_time'
    ];

    public function user(){
        return $this->hasOne('App\Models\User', 'user_id', 'id');
    }

   
}
