<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $fillable = [
        'tag'
    ];
  
    public function clubs(){
      return $this->belongsToMany( 'App\Models\Club', 'clubs_users_tags', 'tag_id', 'user_id');
    }
}
