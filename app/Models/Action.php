<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Action extends Model
{
    protected $table = 'actions';

    public function clubs(){
        return $this->belongsTo( 'App\Models\Club', 'club_id', 'id' );
    }

    public function by(){
        return $this->belongsTo( 'App\Models\User', 'user_id', 'id' );
    }

    public function processedBy(){
        return $this->belongsTo( 'App\Models\User', 'processed_by', 'id' );
    }
}
