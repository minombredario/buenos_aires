<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClubPhoto extends Model
{
  protected $table = 'photos';

  public function club(){
    return $this->belongsTo('App\Models\Club', 'club_id', 'id');
  }

  public function user(){
    return $this->belongsTo('App\Models\User', 'uploaded_by', 'id');
  }
}