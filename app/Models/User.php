<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Utilities\WorkWithDates;
use Storage;
use App\Models\Phone;
use Auth;

class User extends Authenticatable
{
    use HasApiTokens, Notifiable, SoftDeletes;


    
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'active', 'activation_token', 'avatar', 'permission'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'activation_token', 'deleted_at', 'updated_at', 'created_at', 'email_verified_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = ['email_verified_at' => 'datetime'];
    protected $dates = ['deleted_at'];
    protected $appends = ['avatar_url','online', 'last_conection', 'telefonos']; //añado un atributo más de forma virtual


    //relaciones
    public function logged(){
        return $this->hasOne('App\Models\Logged', 'user_id', 'id');
    }

    public function phones(){
        return $this->hasMany( 'App\Models\Phone' );
    }


    //atributos
    public function getAvatarUrlAttribute(){
        return Storage::url('avatars/'.$this->id.'/'.$this->avatar);
    }

    public function getOnlineAttribute(){
        return $this->logged ? $this->logged->online : null;
    }

    public function getLastConectionAttribute(){
        return $this->logged ? WorkWithDates::getLastConnection($this->logged->last_logged_at) : null;
    }

    public function getTelefonosAttribute(){
       
        return $this->mapPhone($this->phones);
    }



    //funciones
    public function authorized(){
       
        abort_unless($this->permission > 0, 401);
        
        return true;
    }

    public function mapPhone($phones){
        $p = array();
        
        foreach ($phones as $key => &$val) {
            array_push($p, $phones[$key]->phone);
        }

        return count($p) ? $p : [''];
    }

    public function syncPhones(array $phones)
    {
        $children = $this->phones;
        $items = collect($phones);

        
        $deleted_ids = $children->filter(function ($phone) use ($items) {
            return empty($items->where('id', $phone->id)->first());
        })->map(function ($phone) {
            $id = $phone->id;
            $phone->delete();
            return $id;
        });
        
        /**
         * Añadí esta sección por si es necesario actualizar
         * los contactos que ya se encuentran relacionados.
         */
        $updates = $items->filter(function ($phone) {
            return isset($phone['id']);
        })->map(function ($phone) {
            $this->phones->map(function ($c) use ($phone) {
                
                $c->updateOrCreate(
                    [ 'id' => $phone['id'] ], [ 'phone' => $phone['phone'] ]
                );
               
            });
        });
        
        $attachments = $items->filter(function ($phone) {
            return !isset($phone['id']) && $phone['phone'] != null;
        })->map(function ($phone) use ($deleted_ids) {
            $phone['id'] = $deleted_ids->pop();
            return $phone;
        })->toArray();
        
        $this->phones()->createMany($attachments);
    }

    


}
