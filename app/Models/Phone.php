<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Phone extends Model
{
    protected $table = 'phones';
    
    protected $fillable = [
        'user_id', 'phone'
    ];

    protected $appends = ['state']; //añado un atributo más de forma virtual

    public function user(){
      return $this->hasOne('App\Models\User', 'user_id', 'id');
    }


    //atributos
    public function getStateAttribute(){
      return $this->state = true;
    }
  }