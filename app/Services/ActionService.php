<?php

namespace App\Services;

use App\Models\Action;
use Auth;

class ActionService{
    public static function createPendingAction( $clubID, $type, $content ){
        $action = new Action();

        $action->club_id 	= $clubID;
        $action->user_id    = Auth::user()->id;
        $action->status     = 0;
        $action->type       = $type;
        $action->content    = json_encode( $content );

        $action->save();
    }

    public static function createApprovedAction( $clubID, $type, $content ){
        $action = new Action();
        
        $action->club_id 	    = $clubID;
        $action->user_id        = Auth::user()->id;
        $action->status         = 1;
        $action->type           = $type;
        $action->content        = json_encode( $content );
        $action->processed_by   = Auth::user()->id;
        $action->processed_on   = date('Y-m-d H:i:s', time() );
        
        $action->save();
    }

    public static function approveAction( $action ){
        
        $action->status       = 1;
        $action->processed_by = Auth::user()->id;
        $action->processed_on = date('Y-m-d H:i:s', time() );
    
        $action->save();
    }
    
    public static function denyAction( $action ){

        $action->status         = 2;
        $action->processed_by   = Auth::user()->id;
        $action->processed_on   = date('Y-m-d H:i:s', time() );
    
        $action->save();
    }
}
?>