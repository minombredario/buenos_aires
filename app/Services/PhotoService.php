<?php

namespace App\Services;

use App\Models\ClubPhoto;
use Auth;
use DB;
use File;

class PhotoService{
    public static function addPhoto( $data, $addedBy, $clubID ){
        if( \Request::hasFile('picture')  ){

            $photo = \Request::file('picture');
            
            if( $photo != null && $photo->isValid() ){
                /*
                    Si la carpeta no existe la creo
                */
                if( !File::exists( app_path().'/Photos/'.$clubID.'/' ) ){
                    File::makeDirectory( app_path() .'/Photos/'.$clubID.'/' );
                }

                /*
                    Establezco la ruta de destino y muevo el archivo allí.
                */
                $destinationPath = app_path().'/Photos/'. $clubID;

                /*
                    Cojo el nombre del archivo y el tipo de archivo
                */
                $filename = time().'-'.$photo->getClientOriginalName();

                /*
                    Lo muevo a la carpeta de destino
                */
                $photo->move( $destinationPath, $filename );

                /*
                    Creo un nuevo registro en la base de datos.
                */
                $clubPhoto = new ClubPhoto();

                $clubPhoto->club_id = $clubID;
                $clubPhoto->uploaded_by = $addedBy;
                $clubPhoto->file_url = $destinationPath . '/' . $filename;

                $clubPhoto->save();
            }
        }

    }

}