<?php

namespace App\Services;


use Illuminate\Support\Facades\Hash;

use Carbon\Carbon;

use App\Models\Clubs;
use App\Models\User;

use DB;
use Auth;
use Avatar;
use Storage;

class UserService{
  /**
   * Updates a user
   * @param App\Models\User $user   User being updated.
   */

    public static function getUsers($permission, $data){

        $search = isset( $data['search'] ) ? $data['search'] : '';
        $orderDirection = isset( $data['direction'] ) ? $data['direction'] : 'ASC';
        $orderBy = isset( $data['order'] ) ? $data['order'] : 'name';
        $perPage = isset( $data['perPage'] ) ? $data['perPage'] : 100;
        $active = $permission > 0 ? '>=' : '=';

        if ( $permission >= 1){
            /*
                Si hay una búsqueda, busco todos los usuarios cuyo nombre 
                coincide con la búsqueda. De lo contrario devuelvo todos los usuarios.
            */
            if( isset( $data['search'] ) ){
                $users = User::where($orderBy , 'LIKE', '%'. $search . '%')
                        ->where('active',$active, 0)
                        ->orderBy($orderBy, $orderDirection)
                        ->paginate($perPage);
                        
            }else{
                $users = User::where('active',$active, 0)
                        ->orderBy($orderBy, $orderDirection)
                        ->paginate($perPage);
            }

            $response = [
                'pagination' => [
                  'total' => $users->total(),
                  'per_page' => $users->perPage(),
                  'current_page' => $users->currentPage(),
                  'last_page' => $users->lastPage(),
                  'from' => $users->firstItem(),
                  'to' => $users->lastItem()
                ],
        
                'items' => $users,
            ];   
    
            return $response;
        }

        abort(403, 'No estás autorizado.');
        //return [['error' => 'No estás autorizado']];
    }

    public static function addUser($permission, $data){
        try{
            DB::beginTransaction();
            
            $data['activation_token'] = str_random(40);
            $data['password'] = bcrypt($data['password']);

            $user = User::create($data);
            
            $avatar = Avatar::create($user->name)->getImageObject()->encode('png');
            Storage::put('avatars/' . $user->id . '/avatar.png', (string) $avatar);

            if( isset( $data['phones'] ) ){
                $user->syncPhones( $data['phones'] );
            }

            
            
            $response = [
                'token' =>  $user->createToken('MyApp')->accessToken,
                'user'  =>  $user,
                'message' =>'Usuario creado con éxito'
            ];
            
            DB::commit();

            return $response;
            
        } catch (Exception $e){
            
            DB::rollBack();
            
            return response()->json($e);
        }
    }

    public static function updateUser( $permission, $data ){
        /*
            Compruebo si estamos actualizando los permisos
        */
        $user = User::findOrFail($data['id']);

        
        try{
            DB::beginTransaction();
            /*
                Comprueba si se permite agregar un administrador por el usuario autenticado.
            */
            if( $data['permission'] == 2 && Auth::user()->can('addAdmins', $permission ) ){
                $user->permission = 2;
            }

            /*
                Comprueba si está permitido agregar un superadministrador por el usuario autenticado.
            */
            if( $data['permission'] == 3 && Auth::user()->can('addSuperAdmins', $permission ) ){
                $user->permission = 3;
            }
            if( isset($data['password']) ){
                $data['password'] = bcrypt($data['password']);
            }
            
            $user->update( $data );

            if( isset( $data['phones'] ) ){
                $user->syncPhones( $data['phones'] );
            }

            $user['message'] = 'Actualizado con éxito'; 
            /*
                Return the user.
            */
            DB::commit();
        } catch (Exception $e){
            DB::rollBack();
            dd($e);
            return response()->json($e);
        }

        return $user->load('phones');
    }
}
