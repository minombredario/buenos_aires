<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Auth;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Logout;


class AuthController extends Controller
{
    //protected $redirectTo = '/home';

    /*public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['getLogin','login','logout', 'register','recover', 'reset','getLogout']]);
    }*/

    public function getUser(){

        return response()->json(Auth::guard('api')->user());
        
    }

    public function getApp(){
        return view('home');
    }

    public function login(Request $request){
        
        $login = $request->input('email');
        $field = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'user';
        
        // validación de los datos del formulario
        if($field == 'email'){
            $this->validate($request, [
                'email'    => 'required|email',
                'password' => 'required|min:8|max:60',
            ],$messages=[]);

            $login = $request->input('email');
        }
        
        
        if($field == 'user'){
            $request->merge(['user' => $login]);
            $this->validate($request, [
                'user'    => 'required',
                'password' => 'required|min:4|max:60',
            ],$messages=[]);
       
            $login = $request->input('user');
            
        }
        
        $credentials = request([$field, 'password']);
        
        if (!Auth::attempt($credentials)) {
        //if (!Auth::guard('api')->attempt($credentials)) {
        
            return response()->json([
                'message' => 'Unauthorized.'], 401);
        }
        
        $user = $request->user();
       
        $tokenResult = $user->createToken('Personal Access Token');
        
        $token = $tokenResult->token;
       
        if ($request->remember_me) {
            $token->expires_at = Carbon::now()->addWeeks(1);
        }
        
        $token->save();
        
        // emitimos evento de Login
        event(new Login(config('auth.defaults.guard'), $user, true));
        
       return response()->json([
            'user'         =>  Auth::guard()->user(),
            'access_token' => $tokenResult->accessToken,
            'token_type'   => 'Bearer',
            'expires_at'   => Carbon::parse(
                $tokenResult->token->expires_at)
                    ->toDateTimeString(),   
        ]);
       
    }

    public function user(Request $request){
        return response()->json($request->user());
    }

    public function logout(){
        auth()->user()->tokens->each(function ($token, $key) {
            $token->delete();
        });

        return response()->json('Logged out successfully', 200);
    }

    public function getLogout(){
        //\Request::user()->token()->revoke();

        
        $user = \Auth::user();
        dd($user);
        // emitimos evento de deslogado
        //event(new Logout(config('auth.defaults.guard'), $user, true));
        //Auth::logout();
        //return response()->json(['message' => 
        //    'Successfully logged out']);
    
        
       // Auth::logout();
       // return redirect('/');
    }

    
}
