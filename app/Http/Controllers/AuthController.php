<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use DB;
class AuthController extends Controller
{
    /**
     * Create user
     *
     * @param  [string] name
     * @param  [string] email
     * @param  [string] password
     * @param  [string] password_confirmation
     * @return [string] message
     */
    public function signup(Request $request){
        $request->validate([
            'name' => 'required|string',
            'email' => 'required|string|email|unique:users',
            'password' => 'required|string|confirmed'
        ]);
        $user = new User([
            'name' => $request->name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
        $user->save();
        return response()->json([
            'message' => 'Successfully created user!'
        ], 201);
    }
  
    public function login(Request $request){
       
        // Compruebe si el usuario con el correo electrónico especificado existe
        $user = User::whereEmail(request('username'))->first();

        if (!$user) {
            return response()->json([
                'message' => 'Correo o contraseña equivocada',
                'status' => 422
            ], 422);
        }

        // Si se encontró un usuario con el correo electrónico, verifique si la contraseña especificada
        // pertenece a este usuario
        if (!Hash::check(request('password'), $user->password)) {
            return response()->json([
                'message' => 'Correo o contraseña equivocada',
                'status' => 422
            ], 422);
        }

        // Enviar una solicitud de API interna para obtener un token de acceso
        $client = DB::table('oauth_clients')
            ->where('password_client', true)
            ->first();

        // Asegúrese de que exista un cliente de contraseña en la base de datos
        if (!$client) {
            return response()->json([
                'message' => 'Laravel Passport no está configurado correctamente.',
                'status' => 500
            ], 500);
        }

        $data = [
            'grant_type' => 'password',
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'username' => request('username'),
            'password' => request('password'),
        ];

        $request = Request::create('/oauth/token', 'POST', $data);

        $response = app()->handle($request);

        // Compruebe si la solicitud fue exitosa
        if ($response->getStatusCode() != 200) {
            return response()->json([
                'message' => 'Correo o contraseña equivocada',
                'status' => 422
            ], 422);
        }

        // Obtener los datos de la respuesta.
        $data = json_decode($response->getContent());

        // Formato de la respuesta final en un formato deseable
        return response()->json([
            'token' => $data->access_token,
            'user' => $user,
            'status' => 200
        ]);
    }
  
    public function logout(Request $request){
        $accessToken = auth()->user()->token();

        $refreshToken = DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);

        $accessToken->revoke();

        return response()->json(['status' => 200]);
    }
  
    public function user(){
       return response()->json(Auth::guard('api')->user());
    }
}