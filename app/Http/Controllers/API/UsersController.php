<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\EditUserRequest;

use App\Models\User;

use App\Services\UserService;

use Illuminate\Http\Request;

use Auth;

class UsersController extends Controller
{

    public function __construct(){
        $this->middleware('auth:api');
    }

    public function updateUser( User $user, EditUserRequest $request ){

        if(Auth::user()->authorized() || $request->get('id') == Auth::user()->id){
            
            $updated = UserService::updateUser( Auth::user()->permission, $request->all() );
                
            return response()->json($updated);
        };

        return response()->json('vacio');
        
    }

}