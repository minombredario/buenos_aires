<?php

namespace App\Http\Controllers\API\Admin;
use App\Utilities\WorkWithDates;
use App\Http\Controllers\Controller;
use App\Models\User;
use App\Models\Logged;
use Auth;
use Illuminate\Http\Request;

use App\Services\UserService;

class UsersController extends Controller
{

    public function __construct(){
        $this->middleware('auth:api');
    }

    public function getUsers( Request $request){
        
        $users = UserService::getUsers( Auth::user()->permission, $request->all() );

        return response()->json($users);
        
    }

    public function getUser(Request $request){

        $user = User::findOrFail($request->get('id'));
        
       return response()->json( $user );
    }

    public function toggleActive ($id){
        $user = User::findOrFail($id);
        $user->active = !$user->active;
        $user->save();
        
        return response()->json($user);
    }
}

