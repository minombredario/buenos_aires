<?php

namespace App\Http\Controllers\API\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use App\Models\Logged;

class StatsController extends Controller
{
    public function usersStats(){
        $active = User::where('active', 1)->count();
        $logged = Logged::where('online', 1)->count();
        $total = User::count();

        $response = [
            'active'    => $active,
            'online'    => $logged,
            'total'     => $total,
        ];

        return response()->json($response);
    }
}
