<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Requests\AddUserRequest;
use App\Http\Controllers\Controller;

use Auth;
use Avatar;
use Storage;
use App\Models\User;
use App\Services\UserService;
use Carbon\Carbon;
use Illuminate\Auth\Events\Login;
use Illuminate\Auth\Events\Logout;
use Illuminate\Support\Facades\Hash;
use DB;

use Validator;
class AuthController extends Controller
{
    /* 
    public function login(Request $request){
       
        // Compruebe si el usuario con el correo electrónico especificado existe
        $user = User::whereEmail(request('email'))->first();

        if (!$user) {
            return response()->json([
                'message' => 'Email equivocado',
                'status' => 422
            ], 422);
        }

        // Si se encontró un usuario con el correo electrónico, verifique si la contraseña especificada
        // pertenece a este usuario
        if (!Hash::check(request('password'), $user->password)) {
            return response()->json([
                'message' => 'Contraseña equivocada',
                'status' => 422
            ], 422);
        }

        // Enviar una solicitud de API interna para obtener un token de acceso
        $client = DB::table('oauth_clients')
            ->where('password_client', true)
            ->first();

        // Asegúrese de que exista un cliente de contraseña en la base de datos
        if (!$client) {
            return response()->json([
                'message' => 'Laravel Passport no está configurado correctamente.',
                'status' => 500
            ], 500);
        }

        $data = [
            'grant_type' => 'password',
            'client_id' => $client->id,
            'client_secret' => $client->secret,
            'username' => request('email'),
            'password' => request('password'),
            'scope' => '',
        ];
                    
        $request = Request::create('/oauth/token', 'POST', $data);

        $response = app()->handle($request);

        // Compruebe si la solicitud fue exitosa
        if ($response->getStatusCode() != 200) {
            return response()->json([
                'message' => 'Correo o contraseña equivocada',
                'status' => 422
            ], 422);
        }

        // Obtener los datos de la respuesta.
        $data = json_decode($response->getContent());

        // emitimos evento de Login
        event(new Login(config('auth.defaults.guard'), $user, true));

        // Formato de la respuesta final en un formato deseable
        return response()->json([
            'expires_in' => $data->expires_in,
            'token' => $data->access_token,
            'refresh_token' => $data->refresh_token,
            'token_type'    => $data->token_type,
            'user' => $user->load('logged'),
            'status' => 200,
        ]);
    }*/
  
    /*public function login(Request $request){
        
        $request->validate([
            'email' => 'required|string|email',
            'password' => 'required|string',
            'remember_me' => 'boolean'
        ]);

        $credentials = request(['email', 'password']);
        $credentials['active'] = 1;
        $credentials['deleted_at'] = null;

        
        if(!Auth::attempt($credentials))
            return response()->json([
                'message' => 'Unauthorized'
            ], 401);

        $user = $request->user();
        $tokenResult = $user->createToken('Personal Access Token');
        $token = $tokenResult->token;
        
        if ($request->remember_me)
            $token->expires_at = Carbon::now()->addWeeks(1);
        $token->save();

        return response()->json([
            'user' => Auth::guard()->user(),
            'token' => $tokenResult->accessToken,
            'token_type' => 'Bearer',
            'expires_at' => Carbon::parse(
                $tokenResult->token->expires_at
            )->toDateTimeString()
        ]);
    }*/
  
    public function login(){
        
        if(Auth::attempt(['email' => request('email'), 'password' => request('password')])){
            $user = Auth::user();

            if($user->permission >= 1){

                $response = [
                    'token' => $user->createToken('MyApp')->accessToken,
                    'user'  => $user,
                ];

                event(new Login(config('auth.defaults.guard'), $user, true));

                return response()->json($response, 200);

            }else{
                return response()->json(['error'=>'Sin autorización'], 401);
            }
            
        }
        else{
            return response()->json(['error'=>'Sin autorización'], 401);
        }
    }

    public function logout(Request $request){
        $accessToken = auth()->user()->token();

        $refreshToken = DB::table('oauth_refresh_tokens')
            ->where('access_token_id', $accessToken->id)
            ->update([
                'revoked' => true
            ]);

        $accessToken->revoke();

        event(new Logout(config('auth.defaults.guard'), Auth::guard('api')->user(), true));

        return response()->json(['status' => 200]);
    }
  
    public function user(Request $request){
        return response()->json(Auth::guard('api')->user());
    }

    public function signupActivate($token){
        $user = User::where('activation_token', $token)->first();
       
        if (!$user) {
            return response()->json([
                'message' => 'This activation token is invalid.'
            ], 404);
        }
        $user->active = true;
        $user->activation_token = '';
        $user->save();

        $avatar = Avatar::create($user->name)->getImageObject()->encode('png');
        Storage::put('avatars/'.$user->id.'/avatar.png', (string) $avatar);
        
        //return view('home');
        return $user;
    }

    public function register(AddUserRequest $request){

        /*if ($validator->fails()) {
             
            return response()->json(['error'=>$validator->errors()], 401);            
        }*/
        if (!$request->expectsJson()) {
            //return back()->withErrors(['field_name' => ['Your custom message here.']]);
            //return redirect()->route('home')->with($msg);
        }

        $newUser = UserService::addUser( Auth::user()->permission, $request->all() );
                
        return response()->json($newUser);
        
    }

}