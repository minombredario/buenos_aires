<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DownloadController extends Controller
{
    public function getDownload(Request $request){
        
        $folder = $request->get('folder');
        $doc = $request->get('doc');
        
        $file= public_path(). "\\" . $folder . "\\" . $doc . '.pdf';
       
        $headers = array(
                'Content-Type: application/pdf',
                );

               
        return \Response::download($file, $request->get('doc') . '.pdf', $headers);

    }
}
