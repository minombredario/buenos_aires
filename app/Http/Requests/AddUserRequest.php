<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Models\User;

class addUserRequest extends FormRequest
{
    public $user;

    public function __construct()
    {
        $this->user = User::findOrFail(auth()->user()->id);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

    public function rules(){
        $permission = $this->user->id == $this->id ? $this->user->permission : ($this->user->permission - 1);
        

        return [
            'name' => 'required',
            'email' => ['required','string','email','max:255','regex:/^.+@.+$/i','unique:users,email'],
            //'email' => ['required','string','email','max:255','regex:/^.+@.+$/i','unique:users,email'],
            'active' => 'required|boolean',
            'permission' => 'required|numeric|between:0,' .  $this->permission,
            //'password' => ['sometimes','min:6','regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\X])(?=.*[!$#%]).*$/'],
            'password' => [
                'required',
                'string',
                'min:6',              // must be at least 10 characters in length
                'regex:/[a-z]/',      // must contain at least one lowercase letter
                'regex:/[A-Z]/',      // must contain at least one uppercase letter
                'regex:/[0-9]/',      // must contain at least one digit
                'regex:/[@$!%*#?&]/', // must contain a special character
            ],
            'c_password' => 'required|same:password',
            'phones.*.phone' => ['nullable','numeric','distinct','regex:/^[9|8|7|6]\d{8}$/'],
        ];
    }

    public function messages(){
        $messages = [];

        foreach(\Request::get('phones') as $key => $val){
            
            $numeral = $key == 0? 'er' : 'º';
            //$messages['phones.' . $key . '.phone.regex'] = 'El ' . ($key+1) . $numeral . ' número de teléfono está mal formado.';
            $messages['phones.' . $key . '.phone.regex'] = 'El teléfono está mal formado.';
            $messages['phones.' . $key . '.phone.unique'] = 'Ha duplicado el teléfono en este usuario';
            $messages['phones.' . $key . '.phone.distinct'] = 'Ha duplicado el teléfono en el formulario';
           
        }

        $messages['permission.between'] = "No estás autorizado para cambiar los permisos";

        return $messages;
    }
}
