<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class SuperAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */

     /*
        Any user with a permission less than 3 is not a super admin and
        receives a 403 un authorized action response.
    */
        
    public function handle($request, Closure $next, $guard = null)
    {
        if (Auth::user()->permission < 3 ) {
            abort(403, 'No estás autorizado.');
        }

        return $next($request);
    }
}