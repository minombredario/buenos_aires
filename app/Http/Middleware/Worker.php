<?php
/*
  Defines the middleware namespace
*/
namespace App\Http\Middleware;

/*
  Defines the facades used by the controller.
*/
use Closure;
use Illuminate\Support\Facades\Auth;

class Worker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($request, Closure $next, $guard = null)
    {
      
      if (Auth::user()->permission < 1 ) {
        
            abort(403, 'No estás autorizado.');
        }

        return $next($request);
    }
}
