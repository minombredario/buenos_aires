<?php

namespace App\Utilities;
use Carbon\Carbon;

class WorkWithDates{
    
    public static function getLastConnection($last_connection){
        return Carbon::createFromTimestamp($last_connection)->diffForHumans();
    }
}