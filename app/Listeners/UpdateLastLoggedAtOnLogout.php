<?php

namespace App\Listeners;

use Illuminate\Auth\Events\Logout;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

/*use Carbon\Carbon;
use App\Models\User;
use App\Models\Logged;
use DB;*/

class UpdateLastLoggedAtOnLogout
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Logout  $event
     * @return void
     */
    public function handle(Logout $event)
    {
        $event->user->logged->online = 0;
        $event->user->logged->last_logged_at = $event->user->logged->current_login_time;
        
        $event->user->logged->save();
 
        // Log::info("ultima conexion: {$event->user->last_logged_at}" );
        
    }
}
