<?php

namespace App\Listeners;

use Carbon\Carbon;
use Illuminate\Auth\Events\Login;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Models\Logged;
class UpdateLastLoggedAtOnLogin
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  Login  $event
     * @return void
     */
    public function handle(Login $event)
    {
        $now = Carbon::now()->timestamp; 
                 
        
       
        if(!is_null($event->user->logged)){
            if (is_null($event->user->logged->last_logged_at)) {
                $event->user->logged->last_logged_at = $now;
            }
    
            $event->user->logged->current_login_time = $now;
            $event->user->logged->online = 1;
     
            $event->user->logged->save();
            
        }else{
            Logged::create([
                'user_id' 		        => $event->user->id,
                'online' 			    => 1,
                'current_login_time' 	=> null,
                'last_logged_at' 		=> null,
            ]);
        }

        
    }
}
