<?php

use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\User::create([
        	'permission' 		=> '3',
            'name' 			    => 'Dario',
	        'email' 			=> 'minombredario@gmail.com',
	        'password' 			=> bcrypt('123'),
	        'remember_token' 	=> str_random(10),
            'activation_token'  => '4CKCp30W8CZfOomsstrbd422efl9DJuJmsZt7TrC'
        ]);

        App\Models\Logged::create([
        	'user_id' 		        => 1,
            'online' 			    => 0,
	        'current_login_time' 	=> null,
	        'last_logged_at' 		=> null,
        ]);
            
    }
}
