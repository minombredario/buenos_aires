<!DOCTYPE html>
<html lang="es">
<head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="csrf-token" content="{{ csrf_token() }}">

        <link href="{{ mix('/css/app.css' ) }}" rel="stylesheet" type="text/css"/>

        <link rel="icon" type="image/x-icon" href="/favicon.ico">
        <link href="https://fonts.googleapis.com/css?family=Lato:300,300i,400,400i,700,700i" rel="stylesheet">
        <link href="https://fonts.googleapis.com/css?family=Josefin+Sans" rel="stylesheet">  
           
        <title>{{ config('app.name', 'AdminPanel') }}</title>
        <script async defer src=<?php echo 'https://maps.googleapis.com/maps/api/js?key=' . env('GOOGLE_MAPS_KEY_JS') ?>></script>
        <script type='text/javascript'>
             window.Laravel = <?php echo json_encode([
                'csrfToken' => csrf_token(),
            ]); ?>
        </script>
    </head>
    <body>
         <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-140790956-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());

            gtag('config', 'UA-140790956-1');
        </script>
       
        <div id="appWeb">
			@yield('content')
		</div>

        <script type="text/javascript" src="{{ mix('js/app.js') }}"></script>
    </body> 
</html>