import Vue from 'vue';
import Vuex from 'vuex';

/*
  Adds the promise polyfill for IE 11
*/

require('es6-promise').polyfill();

Vue.use(Vuex)

import { user }          from './modules/user.js'
//import { filters }      from './modules/filters.js'
import { display }        from './modules/display.js'
import { downloads }  from './modules/downloads.js'

export default new Vuex.Store({
    modules:{
      user,
      //filters,
      display,
      downloads
    },
    
});