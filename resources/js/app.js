/**
 * First we will load all of this project's JavaScript dependencies which
 * includes Vue and other libraries. It is a great starting point when
 * building robust, powerful web applications using Vue and Laravel.
 */

require('./bootstrap');

window.Vue = require('vue');

import Auth from './api/auth.js';
import Api from './api/api.js';
import Swal from 'sweetalert2';


//importar libreria momentjs
import moment from "moment";
window.moment = require('moment');
moment.locale('es');



import BootstrapVue from 'bootstrap-vue';
import router from './rutas/routes.js'
import store from './store.js';
import Chart from 'chart.js';
import { library } from '@fortawesome/fontawesome-svg-core'

//iconos tipos de usuario
import { faUser,faUserPlus,faUsers,faUserAltSlash, faUserCheck,faMale,faFemale, faUserCog, faUserTag } from '@fortawesome/free-solid-svg-icons'

//miscelaneo
import { faIcons, faEnvelope, faPhoneAlt, faMobileAlt, faKey, faMinus, faFilePdf, faFilm, faHome } from '@fortawesome/free-solid-svg-icons'

//iconos botones edición
import { faEdit, faPlus, faEye, faEyeSlash} from '@fortawesome/free-solid-svg-icons'
//listados orden
import { faSortAlphaUp, faSortAlphaDown } from '@fortawesome/free-solid-svg-icons'

import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'


library.add(faUser,faUserPlus,faUsers, faUserCheck, faMale, faFemale, faUserCog, faUserTag, faUserAltSlash)
library.add(faSortAlphaUp, faSortAlphaDown)
library.add(faIcons, faEnvelope, faPhoneAlt, faMobileAlt, faKey, faMinus, faFilePdf, faFilm, faHome)
library.add(faEdit, faPlus, faEye, faEyeSlash)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.component('Loader', require('../js/components/global/Loader.vue').default );


window.Event = new Vue;
window.api = new Api();
window.auth = new Auth();
window.toastr = require('toastr');

Vue.use(BootstrapVue,moment);

import isMobile from './utils/isMobile.js'
Vue.prototype.$isMobile = isMobile;

/*Vue.config.productionTip = false;
Vue.config.devtools = false;
Vue.config.silent = true;*/

Vue.directive('focus', {
  inserted(el) {
      el.focus()
    },
})

const app = new Vue({
    router,
    store,
}).$mount("#app");


/*router.beforeEach((to, from, next) => {
    // redirect to login page if not logged in and trying to access a restricted page
    const publicPages = ['/login'];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = localStorage.getItem('access_token');
    console.log(loggedIn);
    if (authRequired && !loggedIn) {
      return next('/login');
    }
  
    next();
  })*/


/*gtag('set', 'page', router.currentRoute.path);
gtag('send', 'pageview');

router.afterEach(( to, from ) => {
    gtag('set', 'page', to.path);
    gtag('send', 'pageview');
});*/
