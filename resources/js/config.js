var api_url = '';
var google_maps_js_api = process.env.google_maps_js_api;

switch( process.env.NODE_ENV ){
  case 'development':
    api_url = '/api/v1';
  break;
  case 'production':
    api_url = 'http://miweb.es/api/v1';
  break;
}

export const WEB_CONFIG = {
  API_URL: api_url,
  GOOGLE_MAPS_JS_API: google_maps_js_api,
}