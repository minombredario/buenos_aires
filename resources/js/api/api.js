import Auth from "./auth";

class Api {
    constructor () {

    }

    call (requestType, url, data = null) {
        return new Promise((resolve, reject) => {
            axios[requestType](url, data)
                .then( response => {
                    response.data.message ? toastr.success(response.data.message) : '';
                    resolve(response);
                    console.log(response.data);
                    console.log({response});
                }, error => {
                    //auth.logout();
                    /*let errores = error.response.data.error ? error.response.data.error : error.response.data.errors;
                    for (let key in errores){
						toastr.warning(errores[key][0]);
                    }*/
                    /*if(error.response.status === 403){
                        this.$router.push('/admin')
                    }*/
                    if(error.response.status == 403){
                       auth.noAutorizado();
                    }
                                       
                    reject(error);
                    console.log(error.response.data)
                })
                .catch(({response}) => {
                                     
                    if (response.status === 401) {
                        //auth.logout();
                        
                    }
                    console.log(response)
                    reject(response);
                });
        });
    }
}

export default Api;