import router from '../rutas/routes.js';

class Auth {
    constructor () {
        this.token = this.getCookie('access_token') ? this.getCookie('access_token') : window.sessionStorage.getItem('token');
    
        if (this.token) {
            axios.defaults.headers.common['Authorization'] = 'Bearer ' + this.token;
        }
    }

    login (token) {        
        window.sessionStorage.setItem('token', token);
        axios.defaults.headers.common['Authorization'] = 'Bearer ' + token;
        
        this.setCookie('access_token', token);
        this.token = token;
        
        router.push('/layout');
    }

    noAutorizado(){
      router.back();
    }

    logout () {        
        window.sessionStorage.clear()
        this.deleteCookie('access_token');
        axios.defaults.headers.common['Authorization'] = 'Bearer ';

        this.token = null;

        router.push('/login');
        
    }

    check () {
        return !! this.token;
    }

    setCookie(cookie_name, cookie_value, exdays) {
        let d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        let expires = `expires=${d.toUTCString()}`;
        document.cookie = `${cookie_name}=${cookie_value}`;
        document.cookie = `${expires}`;
        document.cookie = `path=/`;
    }

    getCookie(cookie_name) {
      
      let name = `${cookie_name}=`;
      let decodedCookie = decodeURIComponent(document.cookie);
      let ca = decodedCookie.split(';');
      for(let i = 0; i <ca.length; i++) {
        let c = ca[i];
        while (c.charAt(0) == ' ') {
          c = c.substring(1);
        }

        if (c.indexOf(name) == 0) {
          return c.substring(name.length, c.length);
        }
      }

      return "";
    }

    deleteCookie(cookie_name){
      document.cookie = `${cookie_name}=`;
      document.cookie = `expires=Thu, 01 Jan 1970 00:00:00 UTC`;
      document.cookie = `path=/`;
    }

}

export default Auth;