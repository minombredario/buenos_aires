/*
  Imports the Roast API URL from the config.
*/
import { WEB_CONFIG } from '../config.js';

export default {
    getDownload( data ){
        //return api.call('get', `${WEB_CONFIG.API_URL}/getDownload?doc=${data.name}`,{ responseType: 'blob' });
        return axios.get(`${WEB_CONFIG.API_URL}/getDownload?doc=${data.name}&folder=${data.folder}`, { responseType: 'blob' });
    },
}