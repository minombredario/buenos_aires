/*
  Imports the Roast API URL from the config.
*/
import { WEB_CONFIG } from '../config.js';

export default {
    putUpdateUser( data ){
        return api.call('patch', `${WEB_CONFIG.API_URL}/user/${data.id}`, data);
    },

    loginAdmin( data ){
        let formData =  {email: data.email, password: data.password, remember_me: data.remember_me};

        return api.call('post', `${WEB_CONFIG.API_URL}/admin/login`, formData);
        
    },

    login( data ){
        let formData =  {email: data.email, password: data.password, remember_me: data.remember_me};

        return api.call('post', `${WEB_CONFIG.API_URL}/login`, formData);
        
    },

    getUser(){
        return api.call('get', `${WEB_CONFIG.API_URL}/admin/user`);
    },

    
    userLogOut(){
       return api.call('get', `${WEB_CONFIG.API_URL}/logout`);
        
    },
}
