/*
  Imports the Roast API URL from the config.
*/
import { WEB_CONFIG } from '../../config.js';

export default {
    
    getUsersStats(){
      return api.call('get', `${WEB_CONFIG.API_URL}/admin/usersStats`);
      //return axios.get(`${WEB_CONFIG.API_URL}/admin/usersStats`);
    },
    
}
