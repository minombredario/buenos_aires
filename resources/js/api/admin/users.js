/*
  Imports the Roast API URL from the config.
*/
import { WEB_CONFIG } from '../../config.js';

export default {

    userAdminLogOut(){
       return api.call('get', `${WEB_CONFIG.API_URL}/admin/logout`);
        
    },
    
    getUsers( data ){
        return api.call('get', `${WEB_CONFIG.API_URL}/admin/users`, {
            params: data
        });
    },

    loadUser( data ){
        
        return api.call('get', `${WEB_CONFIG.API_URL}/admin/getUser`, {
            params: data
        });
        
        
    },

    newUser( data ) {
        return api.call('post', `${WEB_CONFIG.API_URL}/admin/addUser`, data);
    },

    getAutocompleteUsers( data ){
        return api.call('get', `${WEB_CONFIG.API_URL}/admin/users`, {
            params: data
        });
    },

    

    /*putUpdateUser( data ){
        
        let formData = {permission: data.permission, clubs: data.clubs};

        return axios.put( `${WEB_CONFIG.API_URL}/admin/users/${1}`, formData);
    },*/

    adminLogin( data ){
        let formData =  {username: data.email, password: data.password};
        
        return api.call('post', `${WEB_CONFIG.API_URL}/admin/login`, formData);
        //return axios.post(`${WEB_CONFIG.API_URL}/admin/login`, formData);
    },

    toggleActive(id){

        return api.call('patch',`${WEB_CONFIG.API_URL}/admin/toggleActive/${id}`);
        //return axios.patch(`${WEB_CONFIG.API_URL}/admin/toggleActive/${id}`);
    }
    
}
