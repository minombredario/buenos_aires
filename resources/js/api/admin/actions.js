/*
  Imports the club API URL from the config.
*/
import { WEB_CONFIG } from '../../config.js';

export default {
  getActions(){
    return api.call('get', `${WEB_CONFIG.API_URL}/admin/actions` );
    //return axios.get( `${WEB_CONFIG.API_URL}/admin/actions` );
  },

  putApproveAction( id ){
    return api.call('put', `${WEB_CONFIG.API_URL}/admin/actions/${id}/approve` );
    //return axios.put( `${WEB_CONFIG.API_URL}/admin/actions/${id}/approve` );
  },

  putDenyAction( id ){
    return api.call('put', `${WEB_CONFIG.API_URL}/admin/actions/${id}/deny` );
    //return axios.put( `${WEB_CONFIG.API_URL}/admin/actions/${id}/deny` );
  }
}