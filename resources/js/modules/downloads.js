/*
|-------------------------------------------------------------------------------
| VUEX modules/downloads.js
|-------------------------------------------------------------------------------
| The Vuex data store for the downloads
*/
import DownloadsAPI from '../api/download.js';

export const downloads = {
    state: {
        downloadLoadStatus: 0,
        errores: [],
    },

    actions: {
        getDownload( { commit }, data2 ){
            commit( 'setDownloadLoadStatus', 1 );
            /*
                axios.get(`/api/v1/getDownload?doc=${name}`, { responseType: 'blob' })
                .then(({ data }) => {
                    
                    const blob = new Blob([response], { type: 'application/pdf' })
                    let link = document.createElement('a')
                    link.href = window.URL.createObjectURL(blob)
                    link.download = `${name}.pdf`
                    link.click()
                }, error => {
                    console.log(error.response)
                })
                .catch(response => {
                                        
                console.log(response)
                    
                });
            */
            DownloadsAPI.getDownload( data2 )
                .then( ({ data }) => {
                    
                    const blob = new Blob([data], { type: 'application/pdf' })
                    let link = document.createElement('a')
                    link.href = window.URL.createObjectURL(blob)
                    link.download = `${data2.name}.pdf`
                    link.click()
                }, error => {
                    console.log(error.response)
                    let errores = error.response.data.error ? error.response.data.error : error.response.data.errors;
                    commit( 'setDownloadErrores', errores );
                })
                .catch( catched => {
                    commit( 'setDownloadLoadStatus', 3 );
                    
                });
        }
    },

    mutations: {
        setDownloadErrores( state, errores ){
           state.errores = errores;
        },

        setDownloadLoadStatus( state, status ){
            state.downloadLoadStatus = status;
        },

    },

    getters: {
        getDownloadLoadStatus( state ){
            return state.downloadLoadStatus;
        },

        getDownloadErrores( state ){
            return state.errores;
        },
    }
}
