/*
|-------------------------------------------------------------------------------
| VUEX modules/admin/users.js
|-------------------------------------------------------------------------------
| The Vuex data store for the admin users
*/
import UsersAPI from '../api/user.js';

export const users = {
    state: {
        users: [],
        usersLoadStatus: 0,

        user: {},
        userLoadStatus: 0,
        userUpdateStatus: 0,
    },

    actions: {
        loadUser( { commit }, data ){
            commit( 'setUserLoadStatus', 1 );

            UsersAPI.loadUser( data )
                .then( response => {
                    const token = response.data.token;
                    const user = response.data.user;
                    auth.login(token);
                    commit( 'setUser', user );
                    commit( 'setUserLoadStatus', 2 );
                })
                .catch( catched => {
                    commit( 'setUser', {} );
                    commit( 'setUserLoadStatus', 3 );
            });
        },

        loadActiveUser( { commit } ){
            commit( 'setUserLoadStatus', 1 );
            
            UsersAPI.getUser( )
                .then( response => {
                    commit( 'setUser', response.data );
                    commit( 'setUserLoadStatus', 2 );
                })
                .catch( catched => {
                    commit( 'setUser', {} );
                    commit( 'setUserLoadStatus', 3 );
            });
        },

        userLogOut( { commit }){
            commit( 'setUserLoadStatus', 1 );

            UsersAPI.userLogOut( )
                .then( response => {
                    auth.logout();
                    commit( 'setUser', {} );
                    commit( 'setUserLoadStatus', 3 );
                })
                .catch( catched => {
                    commit( 'setUser', {} );
                    commit( 'setUserLoadStatus', 3 );
            });
        }

    },

    mutations: {
        setUser( state, user ){
            state.user = user;
        },

        setActiveUser( state, user ){
            state.user = user;
        },

        setUserLoadStatus( state, status ){
            state.userLoadStatus = status;
        },

        setUserUpdateStatus( state, status ){
            state.userUpdateStatus = status;
        },

    },

    getters: {
        getUserLoadStatus( state ){
            return () =>{
                return state.userLoadStatus;
            }
        },

        getUsersUpdateStatus( state ){
            return state.userUpdateStatus;
        },

        getUser( state ){
            return state.user;
        },

    }
}
