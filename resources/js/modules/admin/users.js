/*
|-------------------------------------------------------------------------------
| VUEX modules/admin/users.js
|-------------------------------------------------------------------------------
| The Vuex data store for the admin users
*/
import UsersAPI from '../../api/admin/users.js';

export const users = {
    state: {
        userActive: [],
        userActiveLoadStatus: 0,
        userAddStatus: 0,
        
        users: [],
        usersLoadStatus: 0,
        userToggleActionStatus: 0,
        pagination: [],
        pagina: 1,
        usersSearchResults: [],

    },

    actions: {
        autocompleteUsers({ commit, getters }){
            UsersAPI.getUsers( { search : getters.getTextSearch } ).then( response => {
                commit( 'setUsersSearchResults', response.data.items.data );
            }).catch( catched => {
               commit( 'setUsersSearchResults', [] );
            });
        },

        loadUsers( { commit, getters } ){
            commit( 'setUsersLoadStatus', 1 );
            
            getters.getSearchData['page'] = getters.getPage;

            UsersAPI.getUsers( getters.getSearchData ).then( response => {
                commit( 'setUsers', response.data.items );
                commit( 'setPaginate', response.data.pagination );
                commit( 'setUsersLoadStatus', 2 );
            }).catch( catched => {
                commit( 'setUsers', [] );
                commit( 'setUsersLoadStatus', 3 );
            });
        },

        getUser( { commit }, data){
            commit( 'setUserActiveLoadStatus', 1 );
            
            UsersAPI.loadUser( data ).then( response => { 
                response.data.phones.length ? '' : response.data.phones.push({'phone' : '', state: null});
                
                commit( 'setUserActive', response.data );
                commit( 'setUserErrores', [] );
                commit( 'setUserActiveLoadStatus', 2 );
            }, error => {
                let errores = error.response.data.error;
                commit( 'setUserErrores', errores );
            })
            .catch( catched => {
                commit( 'setUserActive', [] );
                commit( 'setUserActiveLoadStatus', 3 );
               
            });
        },

        toggleActive( { commit, state, dispatch }, data ){
            commit( 'setUserToggleActionStatus', 1 );
    
            UsersAPI.toggleActive( data.id ).then( ( response ) => {
                commit( 'setUserToggleActionStatus', 2 );
                dispatch( 'loadUsers', []);
            }).catch( catched => {
                commit( 'setUserToggleActionStatus', 3 );
            });
    
              
        },

        updateAdminUser( { commit }, data ){
            commit( 'setUserUpdateStatus', 1 );
            
            UsersAPI.putUpdateUser( data ).then(  response => {
                commit( 'setUser', response.data );
                commit( 'setUserUpdateStatus', 2 );
            }).catch( catched => {
                commit( 'setUserUpdateStatus', 3 );
            });
        },

        userAdminLogOut( { commit }){
            commit( 'setUserLoadStatus', 1 );

            UsersAPI.userAdminLogOut( ).then( response => {
                auth.logout();
                commit( 'setUser', {} );
                commit( 'setUserLoadStatus', 3 );
            }).catch( catched => {
                commit( 'setUser', {} );
                commit( 'setUserLoadStatus', 3 );
            });
        },

        newUser( { commit }, data ){
            commit( 'setUserAddStatus', 1 );           
            UsersAPI.newUser( data ).then(  response => {
                /*commit( 'setUserActive', response.data );
                if(response.data.id == getters.getUser.id){
                    dispatch( 'loadAdminUser'); 
                }*/
                commit( 'setUserErrores', [] );
                commit( 'setUserAddStatus', 2 );
            }, error => {
                let errores = error.response.data.error ? error.response.data.error : error.response.data.errors;
                commit( 'setUserErrores', errores );
                
            })
            .catch( catched => {
                
                commit( 'setUserActive', {} );
                commit( 'setUserAddStatus', 3 );
            });
        },

    },

    mutations: {
        setUsers(state, users){
            state.users = users;
        },

        setUsersLoadStatus( state, status ){
            state.usersLoadStatus = status;
        },

        setUserAddStatus( state, status ){
            state.userAddStatus = status;
        },

        setUserToggleActionStatus( state, status ){
            state.userToggleActionStatus = status;
        },

        setUsersSearchResults( state, status ){
            state.usersSearchResults = status;
        },

        setUserActive( state, status ){
            state.userActive = status;
        },

        setUserActiveLoadStatus( state, status ){
            state.userActiveLoadStatus = status;
        },

        //Paginate

        setPaginate(state, paginate ){
            state.pagination = paginate;
        },

        setPage( state, page ){
            state.pagina = page;
        },
    },

    getters: {
        getUsers(state){
            return state.users.data;
        },

        getUsersLoadStatus( state ){
            return state.usersLoadStatus;
        },

        getUserAddStatus(state){
            return state.userAddStatus;
        },

        getUserActive(state){
            return state.userActive;
        },

        getUserActiveLoadStatus( state ){
            return state.userActiveLoadStatus;
        },

        getUserToggleStatus( state ){
            return state.userToggleActionStatus;
        },

        getUsersSearchResults ( state ){
            return state.usersSearchResults;
        },

        getUsersPaginate(state){
            return state.pagination;
        },

        getPage( state ){
            return state.pagina;
        }
    
    }
}
