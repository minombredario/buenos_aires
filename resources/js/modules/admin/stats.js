/*
|-------------------------------------------------------------------------------
| VUEX modules/admin/users.js
|-------------------------------------------------------------------------------
| The Vuex data store for the admin stats
*/
import StatsAPI from '../../api/admin/stats.js';

export const stats = {
    state: {
        usersActive: [],
        usersOnline: [],
        usersTotal: [],
    },

    actions: {
        loadUsersStats( { commit } ){
            StatsAPI.getUsersStats().then( response => {

                commit( 'setUsersActive', response.data.active );
                commit( 'setUsersOnline', response.data.online );
                commit( 'setUsersTotal', response.data.total );

            }, error => {
                
            })
            .catch( catched => {
                
                commit( 'setUsersActive', [] );
                commit( 'setUsersOnline', [] );
                commit( 'setUsersTotal', []  );
            });
        },
    },

    mutations: {
        setUsersActive( state, users ){
            state.usersActive = users;
        },

        setUsersOnline( state, users ){
            state.usersOnline = users;
        },

        setUsersTotal( state, users ){
            state.usersTotal = users;
        },

        
    },

    getters: {
        getUsersActive( state ){
            return state.usersActive;
        },

        getUsersOnline( state ){
            return state.usersOnline;
        },

        getUsersTotal( state ){
            return state.usersTotal;
        },
    }
}
