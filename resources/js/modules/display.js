//import isMobile from '../utils/isMobile.js';


export const display = {
    state: {
        showSidebar: 'active',
        showPopOut: false,
        width: 0,
        sidebarWidth: 0,
    },
  
    actions: {
        toggleShowSidebar( { commit, state }, data ){
            commit( 'setShowSidebar', data.showSidebar);
            state.sidebarWidth = data.showSidebar ? this.$isMobile ? 0 : 250 : 80;
        },
    
        toggleShowPopOut( { commit }, data ){
            commit( 'setShowPopOut', data.showPopOut );
        },

        appWidth( { commit }, data ){
            commit( 'setAppWidth', data)
        }
  
    },
  
    mutations: {
        setShowSidebar( state, show){
            state.showSidebar = show;
        },
   
        setShowPopOut( state, show ){
            state.showPopOut = show;
        },

        setAppWidth( state, width ){
            state.width = parseInt(width);
        }
  
    },
  
    getters: {
        getShowSidebar( state ){
            return state.showSidebar;
        },
    
        getShowPopOut( state ){
            return state.showPopOut;
        },
        
        getAppWidth(state){
            let largo = state.width <= 621 && state.sidebarWidth == 250 ? 0 : state.sidebarWidth;
            return state.width - largo;
        }
    }
  }
  