/*
|-------------------------------------------------------------------------------
| VUEX modules/admin/users.js
|-------------------------------------------------------------------------------
| The Vuex data store for the admin users
*/
import UsersAPI from '../api/user.js';

export const user = {
    state: {
        user: {},
        userLoadStatus: 0,
        userUpdateStatus: 0,
        errores: [],

        socio: {},
        socioLoadStatus: 0,
        socioUpdateStatus: 0,
    },

    actions: {
        loginAdmin( { commit }, data ){
            commit( 'setUserLoadStatus', 1 );

            UsersAPI.loginAdmin( data )
                .then( response => {
                    const token = response.data.token;
                    const user = response.data.user;
                    auth.login(token);
                    commit( 'setUser', user );
                    commit( 'setUserErrores', [] );
                    commit( 'setUserLoadStatus', 2 );
                }, error => {
                    //toastr.warning(error.response.data.error);
                    let errores = error.response.data.error ? error.response.data.error : error.response.data.errors;
                    commit( 'setUserErrores', errores );
                    
                })
                .catch( catched => {
                    commit( 'setUser', {} );
                    commit( 'setUserLoadStatus', 3 );
            });
        },
        
        loadAdminUser( { commit } ){
            commit( 'setUserLoadStatus', 1 );
            
            UsersAPI.getUser( )
                .then( response => {
                    commit( 'setUser', response.data );
                    commit( 'setUserLoadStatus', 2 );
                })
                .catch( catched => {
                    commit( 'setUser', {} );
                    commit( 'setUserLoadStatus', 3 );
            });
        },
        
        userLogOut( { commit }){
            commit( 'setUserLoadStatus', 1 );

            UsersAPI.userLogOut( )
                .then( response => {
                    auth.logout();
                    commit( 'setUser', {} );
                    commit( 'setUserLoadStatus', 3 );
                })
                .catch( catched => {
                    commit( 'setUser', {} );
                    commit( 'setUserLoadStatus', 3 );
            });
        },

        updateUser( { commit, getters, dispatch }, data){
            commit( 'setUserActiveLoadStatus', 1 );
            UsersAPI.putUpdateUser( data )
                .then( response => {
                    commit( 'setUserActive', response.data );
                    if(response.data.id == getters.getUser.id){
                        dispatch( 'loadAdminUser'); 
                    }
                    commit( 'setUserErrores', [] );
                    commit( 'setUserActiveLoadStatus', 2 );
                }, error => {
                    let errores = error.response.data.error ? error.response.data.error : error.response.data.errors;
                    commit( 'setUserErrores', errores );
                })
                .catch( catched => {
                    commit( 'setUserActive', {} );
                    commit( 'setUserActiveLoadStatus', 3 );
            });
        }

    },

    mutations: {
        setUser( state, user ){
            state.user = user;
        },

        setUserErrores( state, errores ){
           state.errores = errores;
        },

        setActiveUser( state, user ){
            state.user = user;
        },

        setUserLoadStatus( state, status ){
            state.userLoadStatus = status;
        },

        setUserUpdateStatus( state, status ){
            state.userUpdateStatus = status;
        },

    },

    getters: {
        getUserLoadStatus( state ){
            return () =>{
                return state.userLoadStatus;
            }
        },

        getUserErrores( state ){
            return state.errores;
        },

        getUsersUpdateStatus( state ){
            return state.userUpdateStatus;
        },

        getUser( state ){
            return state.user;
        },

    }
}
