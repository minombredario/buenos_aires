
import variables from "../../sass/abstracts/_variables.scss";

const options =  {
    responsive: true,
    lineTension: 1,
    maintainAspectRatio: false,
    pieceLabel: {
      mode: 'percentage',
      precision: 1
    },
    layout: {
      padding: {
        left: 0,
        right: 0,
        top: 0,
        bottom: 0
      }, 
      background: '#0000'
    },
    legend: {
      display: true,
      postion: 'top',
      fullWidth: false, //este cuadro coge todo el ancho del lienzo (empujando hacia abajo otros cuadros).
      //onClick: newLegendClickHandler,
      onHover:(event) => {
        event.target.style.cursor = 'pointer'
      },
      onLeave: (event) => {
        event.target.style.cursor = 'default';
      },
      reverse: false,
      labels: {
          boxWidth: 40,
          fontSize: 12,
          fontStyle: 'normal',
          fontColor: '#666',
          fontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
          padding: 10,
          usePointStyle: true,
      }
    },
  //  scale: {
      //gridLines: {
        //color: 'black',
     // },
    //  angleLines: {
        //color : 'black'
    //  }
  //  },
    scales: {
      gridLines: { color: "black" },
      yAxis: [{
        ticks: {
          beginAtZero: true,
          padding: 25,
        },
      }],
      yAxes: [{
          display: true,
          scaleLabel: {
            display: true,
          },
          gridLines: {
            display: true,
          },
      }],
      xAxes: [{ 
        display: true,
        scaleLabel: {
          display: true,
        },
        gridLines: {
          display: false,
        },
      }]

    },
    title: {
      position: 'top',
      fontSize: 30,
      fontFamily: "'Helvetica Neue', 'Helvetica', 'Arial', sans-serif",
      fontColor: '#666',
      fontStyle: 'bold',
      padding: 10,
      lineHeight: 1.2,
    }


}

export default options;

/*
  let newLegendClickHandler =  (e, legendItem) => {
    console.log(1);
      let index = legendItem.datasetIndex;
      let ci = this.chart;
      let alreadyHidden = (ci.getDatasetMeta(index).hidden === null) ? false : ci.getDatasetMeta(index).hidden;       
      let anyOthersAlreadyHidden = false;
      let allOthersHidden = true;

      // averiguo el estado actual de las etiquetas
      ci.data.datasets.forEach( (e, i) => {
        let meta = ci.getDatasetMeta(i);
        
        if (i !== index) {
          if (meta.hidden) {
            anyOthersAlreadyHidden = true;
          } else {
            allOthersHidden = false;
          }
        }
      });
      
      // Si la etiqueta en la que hice clic ya está oculta. 
      // entonces quiero mostrar (con otros que ya están ocultos)
      if (alreadyHidden) {
        ci.getDatasetMeta(index).hidden = null;
      } else { 
        // de lo contrario, vamos a descubrir cómo cambiar la visibilidad en función del estado actual
        ci.data.datasets.forEach((e, i) => {
            let meta = ci.getDatasetMeta(i);

            if (i !== index) {
                // maneja la lógica cuando hacemos clic en la etiqueta oculta visible y hay 
                //al menos otra etiqueta que está visible y al menos otra ya oculta 
                //(queremos mantener las que ya están ocultas)
                if (anyOthersAlreadyHidden && !allOthersHidden) {
                    meta.hidden = true;
                } else {
                    // toggle visibility
                    meta.hidden = meta.hidden === null ? !meta.hidden : null;
                }
          } else {
            meta.hidden = null;
          }
        });
      }

      ci.update();
    }
*/