export const filters = {
    state: {
        textSearch: '',
        //onlyLiked: false,
        orderBy: 'name',
        orderDirection: 'ASC',
        perPage: 4,
      
    },
    
    actions: {
      updateSetTextSearch( { commit }, data ){
        commit( 'setTextSearch', data );
      },
  
      updateOrderBy( { commit, state, dispatch }, data ){
        commit( 'setOrderBy', state.data );
        dispatch( 'loadUsers' );
        //dispatch( 'orderUsers', { order: state.orderBy, direction: state.orderDirection } );
      },

      updatePerPage( { commit, state, dispatch }, data ){
        commit( 'setPerPage', state.data );
        dispatch( 'loadUsers' );
      },
  
      updateOrderDirection( { commit, state, dispatch }, data ){
        commit( 'setOrderDirection', data );
        dispatch( 'loadUsers' );
        //dispatch( 'orderUsers', { order: state.orderBy, direction: state.orderDirection } );
      },
  
      resetFilters( { commit }, data ){
        commit( 'resetFilters' );
      }
    },
    
      mutations: {
        setTextSearch( state, search ){
          state.textSearch = search;
        },
  
        setOrderBy( state, orderBy ){
          state.orderBy = orderBy;
        },
    
        setOrderDirection( state, orderDirection ){
          state.orderDirection = orderDirection;
        },
    
        resetFilters( state ){
          state.textSearch = '';
          state.orderBy = 'name';
          state.orderDirection = 'DESC';
          state.perPage = 10;
        },

        setPerPage( state, perPage ){
            state.perPage = perPage;
        }
      },
    
      getters: {
        getTextSearch( state ){
          return state.textSearch;
        },
  
        getOrderBy( state ){
          return state.orderBy;
        },
    
        getOrderDirection( state ){
          return state.orderDirection;
        },

        getPerPage( state ){
            return state.perPage;
        },

        getSearchData( state ){
            return {
                order: state.orderBy,
                direction: state.orderDirection,
                search: state.textSearch,
                perPage: state.perPage,
            }
        }
      }
  }
    