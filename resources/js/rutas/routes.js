import Vue from 'vue';
import VueRouter from 'vue-router'
import store from '../store.js';

Vue.use( VueRouter )

let admin = window.location.pathname == "/admin" ? true : false;

function requireAuth(to, from, next) {
    

    function proceed() {
        if (store.getters.getUserLoadStatus() === 2) {
            if (store.getters.getUser != '') {
                switch( to.meta.permission ){
                    case 'user':
                        next();
                        break;
                    case 'socio':
                        if( store.getters.getUser.permission >= 1 ){
                            next();
                        }else{
                            next('/');
                        }
                        break;
                    case 'admin':
                        if( store.getters.getUser.permission >= 2 ){
                            next();
                        }else{
                            next('/');
                        }
                        break;
                    case 'super-admin':
                            if( store.getters.getUser.permission == 3 ){
                                next();
                            }else{
                                next('/');
                            }
                            break;
                    default:
                        next('login');
                        break;
                }
            } else {
                next('/login');
            }
        }
    }
    
    
    if (to.matched.some(record => record.meta.middlewareAuth)) {     
                
        if (!auth.check() && admin) {
            next({
                path: '/login',
                query: { redirect: to.fullPath }
            });

            return;
        }else{
            proceed();
        }
    }

    if (store.getters.getUserLoadStatus() !== 2) {
        store.dispatch('loadAdminUser');
        
        store.watch(store.getters.getUserLoadStatus, function () {
            if (store.getters.getUserLoadStatus() === 2) {
                proceed();
            }
        });
    } else {
        proceed()
    }
}

let routes = null;


if(!admin){
    routes = [
        {
            path: '/',
            name: 'layout',
            redirect: { name: 'home' },
            component: Vue.component( 'Web', require('../Layouts/Web.vue' ).default ),

            children:[
                {
                    path: '/',
                    name: 'home',
                    component: Vue.component( 'Home', require('../pages/web/home.vue' ).default ),
                },

                {
                    path: '/privat',
                    name: 'acceso_privado',
                    component: Vue.component( 'Privado', require('../pages/web/acceso_privado.vue' ).default ),
                },

                {
                    path: '/anuari',
                    name: 'anuario',
                    component: Vue.component( 'Anuario', require('../pages/web/anuario.vue' ).default ),
                },

                {
                    path: '/comissio',
                    name: 'comision',
                    component: Vue.component( 'Comision', require('../pages/web/comision.vue' ).default ),
                },

                {
                    path: '/descargues',
                    name: 'descargas',
                    component: Vue.component( 'Descargas', require('../pages/web/descargas.vue' ).default ),
                },

                {
                    path: '/esdeveniments',
                    name: 'eventos',
                    component: Vue.component( 'Eventos', require('../pages/web/eventos.vue' ).default ),
                },

                {
                    path: '/galeria',
                    name: 'galeria',
                    component: Vue.component( 'Galeria', require('../pages/web/galeria.vue' ).default ),
                    beforeEnter: requireAuth,
                    meta: {
                        permission: 'socio',
                        middlewareAuth: true,
                    },
                },

                {
                    path: '/llibrets',
                    name: 'llibrets',
                    component: Vue.component( 'Llibrets', require('../pages/web/llibrets.vue' ).default ),

                    children:[
                        {
                            path: '/llibrets/:llibret',
                            name: 'llibret',
                            component: Vue.component( 'llibret', require('../components/web/Llibret.vue' ).default )
                        }
                    ]
                },
                {
                    path: '/articles',
                    name: 'noticias',
                    component: Vue.component( 'Noticias', require('../pages/web/noticias.vue' ).default ),
                },
                {
                    path: '/qui-som',
                    name: 'quienes_somos',
                    component: Vue.component( 'Quienes_somos', require('../pages/web/quienes_somos.vue' ).default ),
                },
            ]
        
        }
    ];
}else{
    routes = [
        {
            path: '/login',
            name: 'login',
            component: Vue.component( 'Login', require('../components/admin/auth/login.vue' ).default ),
            meta: {
            breadCrumb: {name: 'Dashboard', link: 'ja'} //crumb
            },
        },

        {
            path: '/',
            redirect: { name: 'dashboard' },
            name: 'layout',
            component: Vue.component( 'Admin', require('../Layouts/Admin.vue' ).default ),
            beforeEnter: requireAuth,
            meta: {
                permission: 'socio',
                middlewareAuth: true,
                breadCrumb: {name: 'Dashboard', link: '#/'} //crumb
            },
            children:[
                {
                    path: '/',
                    name: 'dashboard',
                    component: Vue.component( 'Dashboard', require('../pages/admin/Dashboard.vue' ).default ),
                    beforeEnter: requireAuth,
                    meta: {
                        permission: 'socio',
                        middlewareAuth: true,
                        breadCrumb: {name: 'Dashboard', link: '#/'} //crumb
                    },
                },
                {
                    path: '/users',
                    name: 'users',
                    component: Vue.component( 'Users', require('../pages/admin/Users/Users.vue' ).default ),
                    beforeEnter: requireAuth,
                    meta: {
                        permission: 'socio',
                        middlewareAuth: true,
                        breadCrumb:  {name: 'Usuarios', link: '#/users' } //crumb
                    },
                },
                {
                    path: '/users/:id',
                    name: 'pageData',
                    component: Vue.component( 'pageData', require('../pages/admin/Users/pageData.vue' ).default ),
                    beforeEnter: requireAuth,
                    meta: {
                        permission: 'socio',
                        middlewareAuth: true,
                        breadCrumb:  {name: 'Usuarios', link: '#/users/:id' } //crumb
                    },
                },

                
                
                {
                    path: '/iconos',
                    name: 'iconos',
                    component: Vue.component( 'Iconos', require('../pages/icons.vue' ).default ),
                    beforeEnter: requireAuth,
                    meta: {
                        permission: 'socio',
                        middlewareAuth: true,
                        breadCrumb:  {name: 'Iconos', link: '#/iconos' } //crumb
                    },
                }
            ]
        },

        { path: '*', redirect: '/' }
    ]
}

let router = new VueRouter({
    hashbang: false,
    linkExactActiveClass: 'active',
    
    root: '/admin/',
    routes
});

export default router;


/*router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.middlewareAuth)) {                
        if (!auth.check()) {
            next({
                path: '/login',
                query: { redirect: to.fullPath }
            });

            return;
        }
    }

    next();
})
*/