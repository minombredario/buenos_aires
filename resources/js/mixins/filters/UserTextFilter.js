export const UserTextFilter = {
    methods: {
        processUserTextFilter( user, text ){
            if( text.length > 0 ){
                /*
                  Si el name, el name de la ubicación, la dirección o la ciudad coinciden con el texto que
                  ha sido añadido, devuelve verdadero si no devuelve falso.
                */
                if(user.name.toLowerCase().match( '[^,]*' + text.toLowerCase() + '[,$]*' )
                    || user.name.toLowerCase().match( '[^,]*' + text.toLowerCase() + '[,$]*' )
                    || user.email.toLowerCase().match( '[^,]*' + text.toLowerCase() + '[,$]*' )
                ){
                  return true;
                }else{
                  return false;
                }
              }else{
                return true;
              }
        }
    }
  }