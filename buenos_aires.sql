-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 19-08-2019 a las 07:40:16
-- Versión del servidor: 10.1.30-MariaDB
-- Versión de PHP: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `buenos_aires`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `actions`
--

CREATE TABLE `actions` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `status` int(11) NOT NULL,
  `processed_by` bigint(20) UNSIGNED DEFAULT NULL,
  `processed_on` datetime DEFAULT NULL,
  `type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `loggeds`
--

CREATE TABLE `loggeds` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `online` tinyint(4) NOT NULL,
  `current_login_time` bigint(20) DEFAULT NULL,
  `last_logged_at` bigint(20) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `loggeds`
--

INSERT INTO `loggeds` (`id`, `user_id`, `online`, `current_login_time`, `last_logged_at`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 1565356672, 1565356672, '2019-08-05 09:53:19', '2019-08-10 07:37:58'),
(2, 2, 0, NULL, NULL, '2019-08-07 15:55:11', '2019-08-07 15:55:11'),
(3, 3, 1, 1565429887, 1565352947, '2019-08-07 15:55:34', '2019-08-10 07:38:07');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2016_06_01_000001_create_oauth_auth_codes_table', 1),
(4, '2016_06_01_000002_create_oauth_access_tokens_table', 1),
(5, '2016_06_01_000003_create_oauth_refresh_tokens_table', 1),
(6, '2016_06_01_000004_create_oauth_clients_table', 1),
(7, '2016_06_01_000005_create_oauth_personal_access_clients_table', 1),
(8, '2019_06_19_091858_added_tags_table', 1),
(9, '2019_06_19_092245_added_photos_table', 1),
(10, '2019_06_19_092420_added_actions_table', 1),
(11, '2019_06_20_094450_added_loggeds_table', 1),
(12, '2019_06_27_113143_added_passwords_fields_table', 1),
(13, '2019_08_15_115518_added_phones_table', 2);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_access_tokens`
--

CREATE TABLE `oauth_access_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_access_tokens`
--

INSERT INTO `oauth_access_tokens` (`id`, `user_id`, `client_id`, `name`, `scopes`, `revoked`, `created_at`, `updated_at`, `expires_at`) VALUES
('05a0c41233fa2a91410d649fbc5ccc495fe0edd8cfc9031493059ac3d79e0bf10dc6370906b86d18', 3, 1, 'MyApp', '[]', 0, '2019-08-10 07:38:08', '2019-08-10 07:38:08', '2020-08-10 09:38:08'),
('0a1f79d81fec38f5d0a375014ba1c106d071de183aa6d76cfe32f1dbfcfc1255e5245418df1af2cb', 1, 1, 'MyApp', '[]', 1, '2019-08-05 09:59:57', '2019-08-05 09:59:57', '2020-08-05 11:59:57'),
('1f6f6310b146343ada73bc85965755a9c67be165598c276687df8ee3e3f8c33896baf9dc7573ed04', 2, 1, 'MyApp', '[]', 0, '2019-08-07 15:55:11', '2019-08-07 15:55:11', '2020-08-07 17:55:11'),
('2589dac02f90d8ed20b37dacb15285d0050d880f5778742c61d09ccf60c57fdfde945daa8d187733', 1, 1, 'MyApp', '[]', 1, '2019-08-09 11:17:52', '2019-08-09 11:17:52', '2020-08-09 13:17:52'),
('3198f0acfeb5da790efc1271de3a7d13c9b81de71d328098348130e001dbe75373fb990f138e5c57', 24, 1, 'MyApp', '[]', 0, '2019-08-10 19:42:35', '2019-08-10 19:42:35', '2020-08-10 21:42:35'),
('38005c66b713b0425172c63debc0f36421250275af4fda1b3e021b3b1b74e98ea0c7a5290b7e7a77', 1, 1, 'MyApp', '[]', 0, '2019-08-07 12:04:06', '2019-08-07 12:04:06', '2020-08-07 14:04:06'),
('6bfb7deae576277de766d14c2266feb41fb278f3212673291697d46e2d4db4f8fc7712116aa3654a', 1, 1, 'MyApp', '[]', 1, '2019-08-09 09:51:16', '2019-08-09 09:51:16', '2020-08-09 11:51:16'),
('9c2b213e960d33127acda8f64f3bf8a501af0e6f9f5c40519170288893431a92153eb52decc7db3b', 3, 1, 'MyApp', '[]', 1, '2019-08-09 10:15:47', '2019-08-09 10:15:47', '2020-08-09 12:15:47'),
('af1b5ae43dd81f7ad38a2bb2bb24f33001e15046e010bb1a4d92224b89b9dd5bfad28bf26709463c', 2, 1, 'MyApp', '[]', 0, '2019-08-07 13:39:32', '2019-08-07 13:39:32', '2020-08-07 15:39:32'),
('b74835fec0ded45a1e3821d8fb34af3e6ad1351f5e93ac32a796802bb1ccfaa9901fa4648fedb564', 7, 1, 'MyApp', '[]', 0, '2019-08-10 07:56:24', '2019-08-10 07:56:24', '2020-08-10 09:56:24'),
('c05b1ff7a54b24f466194c05a4a77865bae31b6bc38267773089738fb11fe61f70318afda83b213a', 1, 1, 'MyApp', '[]', 1, '2019-08-07 08:28:49', '2019-08-07 08:28:49', '2020-08-07 10:28:49'),
('c0de36bc7e5d0d119196771b9302c8fa66510f1de0c005538fe42cf3726ee4cacd5262dcf329d04d', 1, 1, 'MyApp', '[]', 1, '2019-08-09 11:00:45', '2019-08-09 11:00:45', '2020-08-09 13:00:45'),
('cd8fbeafc506689f93e6cdeaf25a5125dfba1078162ba35d3f3b85b43d817deb3d5ac809befa6ab5', 3, 1, 'MyApp', '[]', 0, '2019-08-07 13:42:38', '2019-08-07 13:42:38', '2020-08-07 15:42:38'),
('d2eb0f68faff7420147d4cd590ea08ee6fda5d9e88c820809314c888e8e449e29955cf1a7e9eff89', 1, 1, 'MyApp', '[]', 1, '2019-08-05 11:20:23', '2019-08-05 11:20:23', '2020-08-05 13:20:23'),
('db39661047537f6865d4a810249a723ffd93e32431b5130f6184005698361db8dccf1a54ea7ad4b6', 1, 1, 'MyApp', '[]', 1, '2019-08-07 12:08:44', '2019-08-07 12:08:44', '2020-08-07 14:08:44'),
('e2ddebc75770d2e69b640b58e70693342ba47837c546a99b375146b9669cd86b95308d2afad1f135', 23, 1, 'MyApp', '[]', 0, '2019-08-10 08:45:13', '2019-08-10 08:45:13', '2020-08-10 10:45:13'),
('f4b228458e77fc5bf06d32a7b05594c71c6ca667c02e7f77a244b40bede80a930a216ff77378281e', 3, 1, 'MyApp', '[]', 1, '2019-08-07 15:55:34', '2019-08-07 15:55:34', '2020-08-07 17:55:34');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_auth_codes`
--

CREATE TABLE `oauth_auth_codes` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_id` int(11) NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `scopes` text COLLATE utf8mb4_unicode_ci,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_clients`
--

CREATE TABLE `oauth_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `secret` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `redirect` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `personal_access_client` tinyint(1) NOT NULL,
  `password_client` tinyint(1) NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_clients`
--

INSERT INTO `oauth_clients` (`id`, `user_id`, `name`, `secret`, `redirect`, `personal_access_client`, `password_client`, `revoked`, `created_at`, `updated_at`) VALUES
(1, NULL, 'AdminPanel Personal Access Client', 'bIRm4ZU1flrokhLCyrwG4P4U1yj2XoS6CJ2DsAgs', 'http://localhost', 1, 0, 0, '2019-08-05 09:59:43', '2019-08-05 09:59:43'),
(2, NULL, 'AdminPanel Password Grant Client', 'FUyLDun7Mc4SXYVZq5B8Qc99tnFbMuCkgf2GvLGZ', 'http://localhost', 0, 1, 0, '2019-08-05 09:59:43', '2019-08-05 09:59:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_personal_access_clients`
--

CREATE TABLE `oauth_personal_access_clients` (
  `id` int(10) UNSIGNED NOT NULL,
  `client_id` int(10) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `oauth_personal_access_clients`
--

INSERT INTO `oauth_personal_access_clients` (`id`, `client_id`, `created_at`, `updated_at`) VALUES
(1, 1, '2019-08-05 09:59:43', '2019-08-05 09:59:43');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `oauth_refresh_tokens`
--

CREATE TABLE `oauth_refresh_tokens` (
  `id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `access_token_id` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `revoked` tinyint(1) NOT NULL,
  `expires_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `phones`
--

CREATE TABLE `phones` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `phone` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `phones`
--

INSERT INTO `phones` (`id`, `user_id`, `phone`, `created_at`, `updated_at`) VALUES
(1, 3, 961505411, '2019-08-14 22:00:00', '2019-08-14 22:00:00'),
(2, 3, 961505412, '2019-08-14 22:00:00', '2019-08-14 22:00:00'),
(3, 3, 961505413, '2019-08-14 22:00:00', '2019-08-14 22:00:00'),
(4, 1, 961505410, '2019-08-14 22:00:00', '2019-08-14 22:00:00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `photos`
--

CREATE TABLE `photos` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uploaded_by` bigint(20) UNSIGNED NOT NULL,
  `file_url` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tags`
--

CREATE TABLE `tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tag` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `permission` int(11) NOT NULL DEFAULT '0',
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'avatar.png',
  `active` tinyint(4) NOT NULL DEFAULT '0',
  `activation_token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `deleted_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Volcado de datos para la tabla `users`
--

INSERT INTO `users` (`id`, `permission`, `name`, `email`, `email_verified_at`, `password`, `avatar`, `active`, `activation_token`, `remember_token`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1, 3, 'Dario', 'minombredario@gmail.com', '2019-08-04 22:00:00', '$2y$10$BQcY0FusuLYUPkMA3IodD.Cz0Rjf/yX3xd8Wc7Uu9h4lfAsNd4eL2', 'avatar.png', 1, '4CKCp30W8CZfOomsstrbd422efl9DJuJmsZt7TrC', 'Ppkm0klYvc', '2019-08-05 09:53:19', '2019-08-05 09:53:19', NULL),
(2, 2, 'Nuevo Usuario', 'test@test.com', NULL, '$2y$10$w5zETgifCqvsfWjLou1x1ORo3ZLXp1UynAfK2NYKrDjFxndniXFfO', 'avatar.png', 1, 'sZgVUun734CN6tnatDdWNadTABstCIaI55sJRCXe', NULL, '2019-08-07 13:39:32', '2019-08-10 07:38:51', NULL),
(3, 2, 'Nuevo Usuario 2', 'test2@test.com', NULL, '$2y$10$Fq.W4N.CDM70MUqmkT7O6eAISwgshCxmeRTb8INwZYBMA8FXU3HMK', 'avatar.png', 1, 'QN2JBbFo2iLdLn69YxL1hUDilFQZb2C1I9oVSIaj', NULL, '2019-08-07 13:42:38', '2019-08-09 11:03:55', NULL),
(7, 0, 'Nuevo Usuario 4', 'test4@test.com', NULL, '$2y$10$oaFI/aacKCyCzbd8wpSiRunPk298OjNpY.q9leY.41VlEkYXwSMPG', 'avatar.png', 0, 'y5IGlLDTd41HZkCPB20jiRs4hbWveeCY1zDVYyUd', NULL, '2019-08-10 07:56:24', '2019-08-10 07:56:24', NULL),
(23, 0, 'Nuevo Usuario 3', 'test3@test.com', NULL, '$2y$10$9jk19kKK/eW.Etu4EqJOAu0Lf28euQ4.gAEArEz9/lD0zBCT/Shuq', 'avatar.png', 0, 'Xd2U2ZVEoyxnn0J0IlSpRyyzox1kRWe8e9YWYZyM', NULL, '2019-08-10 08:45:12', '2019-08-10 10:07:58', NULL),
(24, 0, 'Nuevo usuario 5', 'test5@test.com', NULL, '$2y$10$ciKf9c9hq251q08b6XPcPOndEhi.YhG11HUnA69aYX6Uj7xBLL0qS', 'avatar.png', 0, 'OsBNDjWJ4byCDcUskCQAXTJAK4qkaJTRQ3t6oIJg', NULL, '2019-08-10 19:42:34', '2019-08-15 12:27:00', NULL);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `actions`
--
ALTER TABLE `actions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `actions_user_id_foreign` (`user_id`),
  ADD KEY `actions_processed_by_foreign` (`processed_by`);

--
-- Indices de la tabla `loggeds`
--
ALTER TABLE `loggeds`
  ADD PRIMARY KEY (`id`),
  ADD KEY `loggeds_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oauth_access_tokens`
--
ALTER TABLE `oauth_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_access_tokens_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_auth_codes`
--
ALTER TABLE `oauth_auth_codes`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_clients_user_id_index` (`user_id`);

--
-- Indices de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_personal_access_clients_client_id_index` (`client_id`);

--
-- Indices de la tabla `oauth_refresh_tokens`
--
ALTER TABLE `oauth_refresh_tokens`
  ADD PRIMARY KEY (`id`),
  ADD KEY `oauth_refresh_tokens_access_token_id_index` (`access_token_id`);

--
-- Indices de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  ADD PRIMARY KEY (`id`),
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indices de la tabla `phones`
--
ALTER TABLE `phones`
  ADD PRIMARY KEY (`id`),
  ADD KEY `phones_user_id_foreign` (`user_id`);

--
-- Indices de la tabla `photos`
--
ALTER TABLE `photos`
  ADD PRIMARY KEY (`id`),
  ADD KEY `photos_uploaded_by_foreign` (`uploaded_by`);

--
-- Indices de la tabla `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `tags_tag_unique` (`tag`);

--
-- Indices de la tabla `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `actions`
--
ALTER TABLE `actions`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `loggeds`
--
ALTER TABLE `loggeds`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT de la tabla `oauth_clients`
--
ALTER TABLE `oauth_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT de la tabla `oauth_personal_access_clients`
--
ALTER TABLE `oauth_personal_access_clients`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `password_resets`
--
ALTER TABLE `password_resets`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `phones`
--
ALTER TABLE `phones`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `photos`
--
ALTER TABLE `photos`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `tags`
--
ALTER TABLE `tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT de la tabla `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=25;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `actions`
--
ALTER TABLE `actions`
  ADD CONSTRAINT `actions_processed_by_foreign` FOREIGN KEY (`processed_by`) REFERENCES `users` (`id`),
  ADD CONSTRAINT `actions_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `loggeds`
--
ALTER TABLE `loggeds`
  ADD CONSTRAINT `loggeds_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `phones`
--
ALTER TABLE `phones`
  ADD CONSTRAINT `phones_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Filtros para la tabla `photos`
--
ALTER TABLE `photos`
  ADD CONSTRAINT `photos_uploaded_by_foreign` FOREIGN KEY (`uploaded_by`) REFERENCES `users` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
